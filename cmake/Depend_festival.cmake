# - Try to find Festival and EST
# Once done this will define
#
# Festival_FOUND          - set to true if Festival was found
# Festival_LIBRARIES      - link these to use Festival
# Festival_INCLUDE_DIR    - path to Festival header files

include(Tools)

if (Festival_LIBRARIES AND Festival_INCLUDE_DIR)
    # in cache already
    set(Festival_FOUND TRUE)
    message(STATUS "Festival found")
else()

    set(CANDIDATE_LIB_DIR
            /usr/lib
            /usr/local/lib
            /usr/lib/festival
            /usr/local/lib/festival
            /usr/local/festival/lib
            /opt/festival/lib
            )

    set(CANDIDATE_INC_DIR
            /usr/include
            /usr/include/festival
            /usr/local/include
            /usr/local/include/festival
            /usr/local/festival/include
            /opt/festival/include
            )

    find_path(Festival_INCLUDE_DIR festival.h ${CANDIDATE_INC_DIR})
    find_library(Festival_LIBRARIES Festival ${CANDIDATE_LIB_DIR})

    # status output
    include(FindPackageHandleStandardArgs)

    find_package_handle_standard_args(Festival
            REQUIRED_VARS Festival_LIBRARIES Festival_INCLUDE_DIR
            )

    mark_as_advanced(
            Festival_INCLUDE_DIR
            Festival_LIBRARIES
    )

endif()

assert(Festival_INCLUDE_DIR AND Festival_LIBRARIES MESSAGE "Failed to find festival")

execute_process(COMMAND festival --version
        OUTPUT_VARIABLE Festival_VERSION
        ERROR_QUIET)
string(REGEX MATCH "[0-9]+(\\.[0-9]+)*" Festival_VERSION "${Festival_VERSION}")
if(NOT Festival_VERSION)
    set(Festival_VERSION "1.0.0")
    message(STATUS "Could not detect Festival version")
else()
    message(STATUS "Festival version: ${Festival_VERSION}")
endif()

if(Festival_VERSION VERSION_GREATER_EQUAL 2.0)
    message(STATUS "Adding openmp library")
    list(APPEND Festival_LIBRARIES gomp)
endif()