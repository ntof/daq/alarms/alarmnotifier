/*
 * ActionsCollection.h
 *
 *  Created on: May 23, 2018
 *      Author: mdonze
 */

#ifndef ACTIONSCOLLECTION_H_
#define ACTIONSCOLLECTION_H_

#include <vector>

#include <NTOFException.h>

#include "AbstractAction.h"

namespace ntof {
namespace alarm {
namespace notifier {

class ActionsCollection
{
public:
    typedef std::vector<AlarmActionPtr> ActionVector;

    explicit ActionsCollection(const pugi::xml_node &node);
    virtual ~ActionsCollection() = default;

    const ActionVector &getActions() const;
    void executeActions(const pugi::xml_node &payload) const;
    bool empty() const;

private:
    ActionVector actions;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* ACTIONSCOLLECTION_H_ */
