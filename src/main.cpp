/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-08T18:17:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
** Original code by mdonze
**
*/

#include <iostream>
#include <string>
#include <vector>

#include <NTOFException.h>
#include <easylogging++.h>

#include "AlarmNotifier.h"

#include <dis.hxx>

using namespace ntof::alarm;
using std::cout;
using std::endl;

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        std::string configFile = "/etc/ntof/nTOF_AlarmNotifier.xml";
        if (argc > 1)
        {
            configFile = std::string(argv[1]);
        }

        // Get the path of the config file of the logger
        std::string logConfFile = "/etc/ntof/nTOF_AlarmNotifier_logger.conf";
        if (argc > 2)
        {
            logConfFile = argv[2];
        }

        // Initialize logger
        el::Configurations confFromFile(
            logConfFile); // Load configuration from file
        el::Loggers::reconfigureAllLoggers(
            confFromFile); // Re-configures all the loggers to current
                           // configuration file

        LOG(INFO) << "AlarmNotifier starting";
        ntof::alarm::notifier::AlarmNotifier notifier;
        notifier.runner(configFile);
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught NTOFException : " << ex.getMessage();
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught std::exception : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught unknown exception";
    }
    _exit(0);
}
