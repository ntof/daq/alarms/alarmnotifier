/*
 * TimingClient.h
 *
 *  Created on: May 16, 2018
 *      Author: mdonze
 */

#ifndef TIMINGCLIENT_H_
#define TIMINGCLIENT_H_

#include <vector>

#include <DIMXMLInfo.h>

#include "AbstractAction.h"
#include "ActionsCollection.h"

namespace ntof {
namespace alarm {
namespace notifier {

class TimingClient : public dim::DIMXMLInfoHandler
{
public:
    explicit TimingClient(const pugi::xml_node &node);
    ~TimingClient() override;

    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg,
                       const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info) override;

    void connectToTiming();

private:
    typedef std::map<std::string, ActionsCollection *> ActionsMap;
    ActionsMap actions;
    std::string svcName;
    dim::DIMXMLInfo *timingSvc;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* TIMINGCLIENT_H_ */
