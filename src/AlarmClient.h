/*
 * AlarmClient.h
 *
 *  Created on: May 15, 2018
 *      Author: mdonze
 */

#ifndef ALARMCLIENT_H_
#define ALARMCLIENT_H_

#include <chrono>
#include <cstdint>
#include <vector>

#include <DIMXMLInfo.h>
#include <pugixml.hpp>

#include "AlarmDefinition.h"

namespace ntof {
namespace alarm {
namespace notifier {

class AlarmClient : public dim::DIMXMLInfoHandler
{
public:
    typedef std::vector<AlarmDefinition> AlarmDefVector;

    explicit AlarmClient(pugi::xml_node &alarmsNode);
    ~AlarmClient() override;

    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg,
                       const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Connect to the service
     */
    void connectToAlarms();

private:
    /**
     * Try to match the alarm
     * @param alarmNode
     * @return
     */
    AlarmDefinition *getBestMatch(const pugi::xml_node &alarmNode);

    AlarmDefVector alarms;
    std::string alarmService;
    ntof::dim::DIMXMLInfo *info;
    AlarmDefinition *noLinkAlarm;
    std::chrono::milliseconds m_minOffTime;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* ALARMCLIENT_H_ */
