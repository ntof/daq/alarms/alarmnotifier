/*
 * AlarmDefinition.cpp
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#include "AlarmDefinition.h"

#include <iostream>

#include <NTOFException.h>

#include "AbstractAction.h"
#include "utils.hpp"

namespace ntof {
namespace alarm {
namespace notifier {

AlarmDefinition::AlarmDefinition(const pugi::xml_node &node, bool ignore) :
    ident(node.attribute("ident").as_string("")),
    system(node.attribute("system").as_string("")),
    subSystem(node.attribute("subSystem").as_string("")),
    severity(toupper(node.attribute("severity").as_string(""))),
    throttleAttr(node.attribute("throttle").as_string("ident")),
    actions(node),
    ignore(ignore)
{}

const ActionsCollection &AlarmDefinition::getActions() const
{
    return actions;
}

const std::string &AlarmDefinition::getIdentifier() const
{
    return ident;
}

const std::string &AlarmDefinition::getSystem() const
{
    return system;
}

const std::string &AlarmDefinition::getSubSystem() const
{
    return subSystem;
}

const std::string &AlarmDefinition::getSeverity() const
{
    return severity;
}

const std::string &AlarmDefinition::getThrottleAttr() const
{
    return throttleAttr;
}

bool AlarmDefinition::isIgnored() const
{
    return ignore;
}

void AlarmDefinition::setIgnore(bool ignore)
{
    ignore = ignore;
}

bool AlarmDefinition::isThrottled(const std::string &ident)
{
    AlarmThrottleMap::iterator it = m_throttle.find(ident);
    if (it == m_throttle.end())
    {
        return false;
    }
    else if (std::chrono::steady_clock::now() > it->second)
    {
        m_throttle.erase(it);
        return false;
    }
    else
    {
        return true;
    }
}

void AlarmDefinition::throttle(const std::string &ident,
                               const std::chrono::milliseconds &ms)
{
    m_throttle[ident] = std::chrono::steady_clock::now() + ms;
}

void AlarmDefinition::expireThrottle()
{
    // C++11 doesn't support std::erase_if
    const AlarmThrottleMap::mapped_type now(std::chrono::steady_clock::now());
    for (AlarmThrottleMap::iterator it = m_throttle.begin();
         it != m_throttle.end();)
    {
        if (now > it->second)
            it = m_throttle.erase(it);
        else
            ++it;
    }
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
