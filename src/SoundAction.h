/*
 * SoundAction.h
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#ifndef SOUNDACTION_H_
#define SOUNDACTION_H_

#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "AbstractAction.h"

namespace ntof {
namespace alarm {
namespace notifier {

class SoundPlayerThread : public ntof::utils::Thread
{
public:
    SoundPlayerThread();
    void thread_enter() override;
    void thread_func() override;
    void thread_exit() override;
    void playSound(const std::string &file);
    bool isBusy();

private:
    ntof::utils::Queue<std::string *> fileQueue;
    bool busy;
};

class SoundAction : public AbstractAction
{
public:
    explicit SoundAction(const pugi::xml_node &cfgNode);
    ~SoundAction() override = default;
    void executeAction(const pugi::xml_node &payload) override;

private:
    std::string fileName;
    bool enqueue;
    static SoundPlayerThread *playerThread;
};

class SoundActionFactory :
    public AbstractActionFactory,
    public ntof::utils::Singleton<SoundActionFactory>
{
public:
    ~SoundActionFactory() override = default;
    AlarmActionPtr buildAction(const pugi::xml_node &cfgNode) override;

    static SoundActionFactory &init();

protected:
    friend class ntof::utils::Singleton<SoundActionFactory>;

    explicit SoundActionFactory(const std::string &name);
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* SOUNDACTION_H_ */
