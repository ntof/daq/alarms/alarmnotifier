/*
 * ActionsCollection.cpp
 *
 *  Created on: May 23, 2018
 *      Author: mdonze
 */

#include "ActionsCollection.h"

#include <sstream>

namespace ntof {
namespace alarm {
namespace notifier {

ActionsCollection::ActionsCollection(const pugi::xml_node &node)
{
    pugi::xml_node actionsNode = node.child("actions");
    for (pugi::xml_node actionNode = actionsNode.first_child(); actionNode;
         actionNode = actionNode.next_sibling())
    {
        AlarmActionPtr al = AbstractActionFactory::getAction(actionNode);
        if (al.get() != nullptr)
        {
            actions.push_back(al);
        }
        else
        {
            std::string actName = actionNode.attribute("name").as_string("");
            std::ostringstream oss;
            oss << "Action " << actName << " not supported!";
            throw ntof::NTOFException(oss.str(), __FILE__, __LINE__);
        }
    }
}

const ActionsCollection::ActionVector &ActionsCollection::getActions() const
{
    return actions;
}

void ActionsCollection::executeActions(const pugi::xml_node &payload) const
{
    for (const AlarmActionPtr &it : actions)
    {
        it->executeAction(payload);
    }
}

bool ActionsCollection::empty() const
{
    return actions.empty();
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
