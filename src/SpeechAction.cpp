/*
 * SpeechAction.cpp
 *
 *  Created on: May 22, 2018
 *      Author: mdonze
 */

#include "SpeechAction.h"

#include <iostream>

#include <NTOFLogging.hpp>

#include <Singleton.hxx>

// Workaround: abort conflict between festival/EST and cstdlib includded by
// ntofutils More:
// https://stackoverflow.com/questions/8122478/declaration-of-void-abort-throws-different-exceptions
extern "C" void abort_est()
{
    abort();
}
#define abort abort_est
#include <festival/festival.h>
#undef abort

namespace ntof {
namespace alarm {
namespace notifier {

Speecher *Speecher::instance = nullptr;

Speecher *Speecher::getInstance()
{
    if (instance == nullptr)
    {
        instance = new Speecher();
    }
    return instance;
}

Speecher::Speecher() : speechQueue("speechQueue", 50)
{
    start();
}

void Speecher::thread_enter()
{
    // Must be made in the same thread as where we use it
    festival_initialize(1, FESTIVAL_HEAP_SIZE);
    for (std::vector<std::string>::iterator it = initCommands.begin();
         it != initCommands.end(); ++it)
    {
        festival_eval_command(it->c_str());
    }
}

void Speecher::thread_func()
{
    while (speechQueue.count() > 0)
    {
        std::unique_ptr<std::string> text(speechQueue.pop());
        // Say some text;
        LOG(INFO) << "[SpeechAction] saying: " << *text;
        festival_say_text(text->c_str());
    }
}

void Speecher::say(std::string *text)
{
    speechQueue.post(text);
    wake(); // Wake the thread
}

void Speecher::evalCommand(const std::string &cmd)
{
    initCommands.push_back(cmd);
}

SpeechActionText::SpeechActionText(const pugi::xml_node &cfgNode) : type(NONE)
{
    if (std::string(cfgNode.name()) == "text")
    {
        type = PLAIN;
        elName = cfgNode.text().as_string();
    }
    if (std::string(cfgNode.name()) == "variable")
    {
        std::string att = cfgNode.attribute("from").as_string();
        if (att == "attribute")
        {
            type = ATTRIBUTE;
        }
        else if (att == "node")
        {
            type = NODE_TEXT;
        }
        elName = cfgNode.attribute("name").as_string("");
        xPath = cfgNode.attribute("xpath").as_string(".");
    }
}

std::string SpeechActionText::getText(const pugi::xml_node &dataNode)
{
    if (type == PLAIN)
    {
        return elName;
    }
    else
    {
        if (!elName.empty())
        {
            pugi::xpath_node xpathNode = dataNode.select_node(xPath.c_str());
            if (xpathNode)
            {
                pugi::xml_node selectedNode = xpathNode.node();
                if (type == ATTRIBUTE)
                {
                    return selectedNode.attribute(elName.c_str()).as_string();
                }
                else if (type == NODE_TEXT)
                {
                    return selectedNode.text().as_string();
                }
            }
            else
            {
                LOG(WARNING)
                    << "[SpeechAction] node not found by Xpath " << xPath;
            }
        }
    }
    return "";
}

SpeechAction::SpeechAction(const pugi::xml_node &cfgNode) :
    AbstractAction(cfgNode)
{
    for (pugi::xml_node node = cfgNode.first_child(); node;
         node = node.next_sibling())
    {
        texts.push_back(SpeechActionTextPtr(new SpeechActionText(node)));
    }
}

void SpeechAction::executeAction(const pugi::xml_node &payload)
{
    try
    {
        std::stringstream ss;
        for (std::vector<SpeechActionTextPtr>::iterator it = texts.begin();
             it != texts.end(); ++it)
        {
            ss << (*it)->getText(payload);
        }
        std::string *txt = new std::string(ss.str());
        Speecher::getInstance()->say(txt);
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR)
            << "[SpeechAction] unable to execute SpeechAction : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[SpeechAction] unable to execute SpeechAction, "
                      "unsupported exception raised!";
    }
}

SpeechActionFactory::SpeechActionFactory(const std::string &name) :
    AbstractActionFactory(name)
{}

AlarmActionPtr SpeechActionFactory::buildAction(const pugi::xml_node &cfgNode)
{
    return AlarmActionPtr(new SpeechAction(cfgNode));
}

void SpeechActionFactory::configureAction(const pugi::xml_node &cfgNode)
{
    for (pugi::xml_node node = cfgNode.first_child(); node;
         node = node.next_sibling("command"))
    {
        Speecher::getInstance()->evalCommand(
            node.attribute("value").as_string());
    }
}
SpeechActionFactory &SpeechActionFactory::init()
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
        delete m_instance;

    m_instance = new SpeechActionFactory("speech");
    return *m_instance;
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
