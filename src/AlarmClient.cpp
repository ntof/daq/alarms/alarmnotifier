/*
 * AlarmClient.cpp
 *
 *  Created on: May 15, 2018
 *      Author: mdonze
 */

#include "AlarmClient.h"

#include <algorithm>
#include <iostream>

#include <easylogging++.h>

#include "AlarmDefinition.h"
#include "utils.hpp"

namespace ntof {
namespace alarm {
namespace notifier {

AlarmClient::AlarmClient(pugi::xml_node &alarmsNode) :
    alarmService(alarmsNode.attribute("service").as_string("Alarms")),
    m_minOffTime(alarmsNode.attribute("lastActive").as_llong(2000)),
    info(nullptr),
    noLinkAlarm(nullptr)
{
    // Gets list of alarms to be ignored
    pugi::xml_node ignoreNode = alarmsNode.child("ignore");
    for (pugi::xml_node toIgnore = ignoreNode.first_child(); toIgnore;
         toIgnore = toIgnore.next_sibling())
    {
        alarms.push_back(AlarmDefinition(toIgnore, true));
    }

    // Gets list of alarm to be overridden
    pugi::xml_node overrideNode = alarmsNode.child("override");
    for (pugi::xml_node toOverride = overrideNode.first_child(); toOverride;
         toOverride = toOverride.next_sibling())
    {
        alarms.push_back(AlarmDefinition(toOverride));
    }

    // Gets defaults
    pugi::xml_node defaultNode = alarmsNode.child("default");
    for (pugi::xml_node defaultSev = defaultNode.first_child(); defaultSev;
         defaultSev = defaultSev.next_sibling())
    {
        alarms.push_back(AlarmDefinition(defaultSev));
    }
}

AlarmClient::~AlarmClient()
{
    delete info;
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param info DIMXMLInfo object who made this callback
 */
void AlarmClient::errorReceived(std::string /*errMsg*/,
                                const ntof::dim::DIMXMLInfo * /*info*/)
{}

/**
 * Callback when a new DIMXMLInfo is received
 * @param doc XML document containing new data
 * @param info DIMXMLInfo object who made this callback
 */
void AlarmClient::dataReceived(pugi::xml_document &doc,
                               const ntof::dim::DIMXMLInfo * /*info*/)
{
    ntof::utils::Timestamp now;
    pugi::xml_node rootNode = doc.first_child();
    pugi::xml_node alarmsNode = rootNode.child("alarms");
    for (pugi::xml_node alarmNode = alarmsNode.first_child(); alarmNode;
         alarmNode = alarmNode.next_sibling())
    {
        const std::string ident = alarmNode.attribute("ident").as_string("");
        // This will ignore masked or disabled alarms
        if (alarmNode.attribute("masked").as_bool(false) ||
            alarmNode.attribute("disabled").as_bool(false))
        {
            LOG(INFO) << "[AlarmClient] alarm masked/disabled: " << ident;
            continue;
        }

        AlarmDefinition *def = getBestMatch(alarmNode);
        if (def == nullptr)
        {
            LOG(DEBUG) << "[AlarmClient] no definition for alarm " << ident;
            continue;
        }

        const std::string throttleAttr =
            alarmNode.attribute(def->getThrottleAttr().c_str()).as_string("");
        if (def->isThrottled(throttleAttr))
        {
            LOG(INFO) << "[AlarmClient] alarm throttled: " << ident;
            continue;
        }
        else if (!def->isIgnored())
        {
            const ActionsCollection &actions = def->getActions();
            if (actions.empty())
            {
                LOG(INFO)
                    << "[AlarmClient] no actions for alarm "
                    << def->getIdentifier() << "(" << def->getSeverity() << ")";
            }
            else
            {
                LOG(INFO)
                    << "[AlarmClient] running actions for alarm on " << ident;
                actions.executeActions(alarmNode);
            }
            def->throttle(throttleAttr, m_minOffTime);
        }
        else
        {
            LOG(INFO) << "[AlarmClient] alarm on " << ident << " ignored";
        }
    }

    for (AlarmDefinition &it : alarms)
    {
        it.expireThrottle();
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param info DIMXMLInfo object who made this callback
 */
void AlarmClient::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    if (noLinkAlarm != nullptr)
    {
        const ActionsCollection &actions = noLinkAlarm->getActions();
        if (!actions.empty())
        {
            actions.executeActions(pugi::xml_node());
        }
    }
}

/**
 * Connect to the service
 */
void AlarmClient::connectToAlarms()
{
    info = new ntof::dim::DIMXMLInfo(alarmService, this);
}

AlarmDefinition *AlarmClient::getBestMatch(const pugi::xml_node &alarmNode)
{
    AlarmDefinition *ret = nullptr;

    std::string ident = alarmNode.attribute("ident").as_string("");
    std::string severityStr = toupper(
        alarmNode.attribute("severityStr").as_string(""));
    std::string system = alarmNode.attribute("system").as_string("");
    std::string subsystem = alarmNode.attribute("subsystem").as_string("");

    for (AlarmDefinition &it : alarms)
    {
        // If identifier match, return now
        if (it.getIdentifier() == ident)
        {
            return &it;
        }
        else if (it.getSystem() == system)
        {
            if (it.getSubSystem() == subsystem)
            {
                // Subsystem and system match;
                ret = &it;
            }
            // System match, at least good
            if (ret == nullptr)
            {
                ret = &it;
            }
        }
        else if (it.getSeverity() == severityStr)
        {
            if (ret == nullptr)
            {
                // Not yet set, maybe it's a default alarm
                ret = &it;
            }
        }
    }
    return ret;
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
