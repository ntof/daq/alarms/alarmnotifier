/*
 * AlarmDefinition.h
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#ifndef ALARMDEFINITION_H_
#define ALARMDEFINITION_H_

#include <chrono>
#include <cstdint>
#include <string>
#include <vector>

#include <Timestamp.h>
#include <pugixml.hpp>

#include "AbstractAction.h"
#include "ActionsCollection.h"

namespace ntof {
namespace alarm {
namespace notifier {

class AlarmDefinition
{
public:
    typedef std::map<std::string,
                     std::chrono::time_point<std::chrono::steady_clock>>
        AlarmThrottleMap;

    explicit AlarmDefinition(const pugi::xml_node &node, bool ignore = false);
    ~AlarmDefinition() = default;

    const ActionsCollection &getActions() const;
    const std::string &getIdentifier() const;
    const std::string &getSystem() const;
    const std::string &getSubSystem() const;
    const std::string &getSeverity() const;
    const std::string &getThrottleAttr() const;
    bool isIgnored() const;
    void setIgnore(bool ignore);

    bool isThrottled(const std::string &ident);
    void throttle(const std::string &ident, const std::chrono::milliseconds &ms);
    void expireThrottle();

private:
    std::string ident;     //!< Alarm identifier
    std::string system;    //!< Alarm system
    std::string subSystem; //!< Alarm subSystem
    std::string severity;  //!< Alarm severity
    std::string throttleAttr;
    ActionsCollection actions; //!< Actions to be performed on raised
    bool ignore;               //!< Do we ignore this alarm?

    AlarmThrottleMap m_throttle;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* ALARMDEFINITION_H_ */
