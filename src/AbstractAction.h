/*
 * AlarmAction.h
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#ifndef ABSTRACTACTION_H_
#define ABSTRACTACTION_H_

#include <map>
#include <memory>

#include <pugixml.hpp>

namespace ntof {
namespace alarm {
namespace notifier {

/**
 * Abstract alarm action to be made
 */
class AbstractAction
{
public:
    explicit AbstractAction(const pugi::xml_node &cfgNode);
    virtual ~AbstractAction() = default;
    virtual void executeAction(const pugi::xml_node &payload) = 0;
    const std::string &getActionName();

private:
    virtual void loadConfiguration(const pugi::xml_node &cfgNode);
    std::string actionName;
};

typedef std::shared_ptr<AbstractAction> AlarmActionPtr;

class AbstractActionFactory
{
public:
    typedef std::map<std::string, AbstractActionFactory *> FactoryMap;

    explicit AbstractActionFactory(const std::string &name);
    virtual ~AbstractActionFactory() = default;

    virtual AlarmActionPtr buildAction(const pugi::xml_node &cfgNode) = 0;
    virtual void configureAction(const pugi::xml_node &cfgNode);

    static void setActionConfiguration(const std::string &name,
                                       const pugi::xml_node &cfgNode);
    static AlarmActionPtr getAction(const pugi::xml_node &cfgNode);

private:
    static FactoryMap *factories;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* ABSTRACTACTION_H_ */
