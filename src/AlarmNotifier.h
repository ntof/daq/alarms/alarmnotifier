/*
 * AlarmNotifier.h
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#ifndef ALARMNOTIFIER_H_
#define ALARMNOTIFIER_H_

#include <string>
#include <vector>

namespace ntof {
namespace alarm {
namespace notifier {

class AlarmClient;
class AlarmDefinition;
class StateClient;
class TimingClient;

class AlarmNotifier
{
public:
    AlarmNotifier();
    virtual ~AlarmNotifier();
    void loadConfiguration();

    /**
     * Main entry point from main
     */
    [[noreturn]] void runner(const std::string &configFile);

    /**
     * Performs clean-up. For being called in main thread
     */
    void cleanUp();
    void connect();

private:
    AlarmClient *alarmClient;
    TimingClient *timingClient;
    std::vector<StateClient *> stateClients;

    std::string m_configPath;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* ALARMNOTIFIER_H_ */
