/*
 * TimingClient.cpp
 *
 *  Created on: May 16, 2018
 *      Author: mdonze
 */

#include "TimingClient.h"

#include <iostream>
#include <sstream>

#include <NTOFException.h>
#include <easylogging++.h>

namespace ntof {
namespace alarm {
namespace notifier {

TimingClient::TimingClient(const pugi::xml_node &node) :
    svcName(node.attribute("service").as_string("")), timingSvc(nullptr)
{
    if (svcName.empty())
    {
        LOG(WARNING)
            << "Missing service attribute for configuring timing client. "
               "using defaults 'Timing/event'";
        svcName = "Timing/event";
    }
    pugi::xml_node valuesNode = node.child("values");
    for (pugi::xml_node valueNode = valuesNode.first_child(); valueNode;
         valueNode = valueNode.next_sibling())
    {
        std::string name = valueNode.attribute("name").as_string("");
        ActionsCollection *actions = new ActionsCollection(valueNode);
        this->actions[name] = actions;
    }
}

TimingClient::~TimingClient()
{
    for (ActionsMap::iterator it = actions.begin(); it != actions.end(); ++it)
    {
        ActionsCollection *vect = it->second;
        delete vect;
    }
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param info DIMXMLInfo object who made this callback
 */
void TimingClient::errorReceived(std::string /*errMsg*/,
                                 const ntof::dim::DIMXMLInfo * /*info*/)
{}

/**
 * Callback when a new DIMXMLInfo is received
 * @param doc XML document containing new data
 * @param info DIMXMLInfo object who made this callback
 */
void TimingClient::dataReceived(pugi::xml_document &doc,
                                const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(INFO) << "[TimingClient] timing event";
    pugi::xml_node eventNode = doc.first_child().child("event");
    std::string eventName = eventNode.attribute("name").as_string("");
    ActionsMap::iterator it = actions.find(eventName);
    if (it != actions.end())
    {
        ActionsCollection *vect = it->second;
        vect->executeActions(eventNode);
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param info DIMXMLInfo object who made this callback
 */
void TimingClient::noLink(const ntof::dim::DIMXMLInfo * /*info*/) {}

void TimingClient::connectToTiming()
{
    timingSvc = new ntof::dim::DIMXMLInfo(svcName, this);
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
