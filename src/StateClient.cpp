/*
 * StateClient.cpp
 *
 *  Created on: May 15, 2018
 *      Author: mdonze
 */

#include "StateClient.h"

#include <iostream>

#include <easylogging++.h>

#include "AlarmDefinition.h"

namespace ntof {
namespace alarm {
namespace notifier {

StateClient::StateClient(pugi::xml_node &cfgNode) :
    stateClient(nullptr),
    svcName(cfgNode.attribute("service").as_string("")),
    defaultStateActions(nullptr),
    allStateActions(nullptr),
    previousState(-255)
{
    if (svcName.empty())
    {
        LOG(ERROR) << "[StateClient] Missing service attribute for configuring "
                      "state client";
    }
    pugi::xml_node defaultNode = cfgNode.child("default");
    if (defaultNode)
    {
        defaultStateActions = new ActionsCollection(defaultNode);
    }
    pugi::xml_node allNode = cfgNode.child("all");
    if (allNode)
    {
        allStateActions = new ActionsCollection(allNode);
    }

    pugi::xml_node valuesNode = cfgNode.child("values");
    for (pugi::xml_node valueNode = valuesNode.first_child(); valueNode;
         valueNode = valueNode.next_sibling())
    {
        int val = valueNode.attribute("enum").as_int();
        states[val] = new ActionsCollection(valueNode);
    }
}

StateClient::~StateClient()
{
    for (ActionsMap::iterator it = states.begin(); it != states.end(); ++it)
    {
        delete it->second;
    }
    delete defaultStateActions;
}

/**
 * Callback when an error is made by XML parser
 * @param errMsg Error message in string
 * @param info DIMXMLInfo object who made this callback
 */
void StateClient::errorReceived(std::string /*errMsg*/,
                                const ntof::dim::DIMXMLInfo * /*info*/)
{}

/**
 * Callback when a new DIMXMLInfo is received
 * @param doc XML document containing new data
 * @param info DIMXMLInfo object who made this callback
 */
void StateClient::dataReceived(pugi::xml_document &doc,
                               const ntof::dim::DIMXMLInfo * /*info*/)
{
    pugi::xml_node rootNode = doc.first_child();
    pugi::xml_attribute valAttr = rootNode.attribute("value");
    pugi::xml_attribute valStrAtt = rootNode.attribute("strValue");
    if (valAttr && valStrAtt)
    {
        int stateValue = valAttr.as_int();
        if (stateValue != previousState)
        {
            previousState = stateValue;
            std::string stateString = valStrAtt.as_string();
            LOG(INFO) << "[StateClient] new state on " << svcName << " "
                      << stateString << " (" << stateValue << ")";

            ActionsMap::iterator it = states.find(stateValue);
            ActionsCollection *state = (it != states.end()) ?
                it->second :
                defaultStateActions;
            if (state != nullptr)
            {
                state->executeActions(rootNode);
            }
            if (allStateActions != nullptr)
            {
                allStateActions->executeActions(rootNode);
            }
        }
    }
}

/**
 * Callback when a not link is present on DIMXMLInfo
 * @param info DIMXMLInfo object who made this callback
 */
void StateClient::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[StateClient] no link on " << svcName;
}

/**
 * Connects to DIM
 */
void StateClient::connect()
{
    if (!svcName.empty())
    {
        LOG(INFO) << "[StateClient] connecting to " << svcName;
        stateClient = new dim::DIMXMLInfo(svcName, this);
    }
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
