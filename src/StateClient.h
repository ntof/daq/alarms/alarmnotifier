/*
 * StateClient.h
 *
 *  Created on: May 15, 2018
 *      Author: mdonze
 */

#ifndef STATECLIENT_H_
#define STATECLIENT_H_

#include <map>

#include <DIMStateClient.h>
#include <pugixml.hpp>

#include "AbstractAction.h"
#include "ActionsCollection.h"

namespace ntof {
namespace alarm {
namespace notifier {

class StateClient : public dim::DIMXMLInfoHandler
{
public:
    typedef std::map<int, ActionsCollection *> ActionsMap;

    explicit StateClient(pugi::xml_node &cfgNode);
    ~StateClient() override;

    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg,
                       const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info) override;

    /**
     * Connects to DIM
     */
    void connect();

private:
    dim::DIMXMLInfo *stateClient;
    std::string svcName;
    ActionsMap states;
    ActionsCollection *defaultStateActions;
    ActionsCollection *allStateActions;
    int previousState;
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* STATECLIENT_H_ */
