/*
 * AlarmAction.cpp
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#include "AbstractAction.h"

#include <iostream>

#include <easylogging++.h>

namespace ntof {
namespace alarm {
namespace notifier {

AbstractActionFactory::FactoryMap *AbstractActionFactory::factories = nullptr;

AbstractAction::AbstractAction(const pugi::xml_node &cfgNode) :
    actionName(cfgNode.attribute("name").as_string())
{
    // loadConfiguration(cfgNode);
}

/**
 * Gets the name of this action
 * @return
 */
const std::string &AbstractAction::getActionName()
{
    return actionName;
}

void AbstractAction::loadConfiguration(const pugi::xml_node & /*cfgNode*/) {}

AbstractActionFactory::AbstractActionFactory(const std::string &name)
{
    if (factories == nullptr)
    {
        factories = new FactoryMap();
    }
    (*factories)[name] = this;
    LOG(INFO) << "Registering action " << name;
}

AlarmActionPtr AbstractActionFactory::getAction(const pugi::xml_node &cfgNode)
{
    std::string actionName = cfgNode.attribute("name").as_string();
    if (factories != nullptr)
    {
        FactoryMap::iterator it = factories->find(actionName);
        if (it != factories->end())
        {
            return it->second->buildAction(cfgNode);
        }
    }
    return AlarmActionPtr();
}

void AbstractActionFactory::configureAction(const pugi::xml_node & /*cfgNode*/)
{}

void AbstractActionFactory::setActionConfiguration(const std::string &name,
                                                   const pugi::xml_node &cfgNode)
{
    if (factories != nullptr)
    {
        FactoryMap::iterator it = factories->find(name);
        if (it != factories->end())
        {
            it->second->configureAction(cfgNode);
        }
        else
        {
            LOG(ERROR) << "Unable to configure action " << name
                       << " it doesn't exists!";
        }
    }
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
