/*
 * SoundAction.cpp
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#include "SoundAction.h"

#include <chrono>
#include <iostream>
#include <thread>

#include <NTOFLogging.hpp>
#include <SDL2/SDL_mixer.h>

#include "AbstractAction.h"

#include <Singleton.hxx>

namespace ntof {
namespace alarm {
namespace notifier {

SoundPlayerThread::SoundPlayerThread() :
    fileQueue("SoundQueue", 10), busy(false)
{}

void SoundPlayerThread::thread_enter()
{
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) ==
        -1)
    {
        LOG(ERROR)
            << "[SoundAction] error while opening mixer " << Mix_GetError();
        return;
    }
}

void SoundPlayerThread::thread_func()
{
    while (fileQueue.count() > 0)
    {
        std::unique_ptr<std::string> fileName(fileQueue.pop());
        Mix_Music *music = Mix_LoadMUS(fileName->c_str());
        if (!music)
        {
            LOG(ERROR)
                << "[SoundAction] unable to play file : " << Mix_GetError();
        }
        else
        {
            LOG(INFO) << "[SoundAction] playing: " << *fileName;
            busy = true;
            Mix_PlayMusic(music, 1);
            while (Mix_PlayingMusic() == 1)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
            busy = false;
            Mix_FreeMusic(music);
        }
    }
}

void SoundPlayerThread::thread_exit()
{
    Mix_CloseAudio(); // Closing mixer
}

void SoundPlayerThread::playSound(const std::string &file)
{
    try
    {
        fileQueue.post(new std::string(file));
        wake(); // Wake the thread
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR) << "[SoundAction] unable to play sound " << ex.what();
    }
}

bool SoundPlayerThread::isBusy()
{
    return busy;
}

SoundPlayerThread *SoundAction::playerThread(nullptr);

SoundAction::SoundAction(const pugi::xml_node &cfgNode) :
    AbstractAction(cfgNode),
    fileName(cfgNode.attribute("file").as_string("")),
    enqueue(cfgNode.attribute("enqueue").as_int(1))
{}

void SoundAction::executeAction(const pugi::xml_node & /*payload*/)
{
    try
    {
        if (playerThread == nullptr)
        {
            playerThread = new SoundPlayerThread();
            playerThread->start();
        }
        if ((enqueue == 0) && playerThread->isBusy())
        {
            LOG(WARNING) << "[SoundAction] player is busy, don't play sound";
            return;
        }
        playerThread->playSound(fileName);
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR)
            << "[SoundAction] unable to execute SpeechAction : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[SoundAction] unable to execute SpeechAction, "
                      "unsupported exception raised!";
    }
}

SoundActionFactory::SoundActionFactory(const std::string &name) :
    AbstractActionFactory(name)
{}

AlarmActionPtr SoundActionFactory::buildAction(const pugi::xml_node &cfgNode)
{
    return AlarmActionPtr(new SoundAction(cfgNode));
}

SoundActionFactory &SoundActionFactory::init()
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
        delete m_instance;

    m_instance = new SoundActionFactory("sound");
    return *m_instance;
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
