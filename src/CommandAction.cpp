/*
 * CommandAction.cpp
 *
 *  Created on: May 29, 2018
 *      Author: mdonze
 */

#include "CommandAction.h"

#include <cstdlib>
#include <iostream>

#include <NTOFException.h>
#include <NTOFLogging.hpp>

#include "AbstractAction.h"

#include <Singleton.hxx>

namespace ntof {
namespace alarm {
namespace notifier {

CommandExecutorThread::CommandExecutorThread() : fileQueue("CommandQueue", 10)
{}

void CommandExecutorThread::thread_func()
{
    while (fileQueue.count() > 0)
    {
        std::unique_ptr<std::string> cmdName(fileQueue.pop());
        int ret = system(cmdName->c_str());
        if (ret != 0)
        {
            LOG(ERROR) << "[CommandAction] command " << *cmdName
                       << " doesn't return 0!";
        }
    }
}

void CommandExecutorThread::executeCommand(const std::string &file)
{
    try
    {
        fileQueue.post(new std::string(file));
        wake(); // Wake the thread
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR) << "[CommandAction] unable to play sound " << ex.what();
    }
}

CommandExecutorThread *CommandAction::executorThread(nullptr);

CommandAction::CommandAction(const pugi::xml_node &cfgNode) :
    AbstractAction(cfgNode),
    commandLine(cfgNode.attribute("command").as_string(""))
{}

void CommandAction::executeAction(const pugi::xml_node & /*payload*/)
{
    try
    {
        if (executorThread == nullptr)
        {
            executorThread = new CommandExecutorThread();
            executorThread->start();
        }
        executorThread->executeCommand(commandLine);
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR)
            << "[SpeechAction] unable to execute SpeechAction : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[SpeechAction] unable to execute SpeechAction, "
                      "unsupported exception raised!";
    }
}

CommandActionFactory::CommandActionFactory(const std::string &name) :
    AbstractActionFactory(name)
{}

AlarmActionPtr CommandActionFactory::buildAction(const pugi::xml_node &cfgNode)
{
    return AlarmActionPtr(new CommandAction(cfgNode));
}

CommandActionFactory &CommandActionFactory::init()
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
        delete m_instance;

    m_instance = new CommandActionFactory("command");
    return *m_instance;
}

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */
