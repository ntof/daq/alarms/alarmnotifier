/*
 * CommandAction.h
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */

#ifndef COMMANDACTION_H_
#define COMMANDACTION_H_

#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "AbstractAction.h"

namespace ntof {
namespace alarm {
namespace notifier {

class CommandExecutorThread : public ntof::utils::Thread
{
public:
    CommandExecutorThread();
    void thread_func() override;
    void executeCommand(const std::string &file);

private:
    ntof::utils::Queue<std::string *> fileQueue;
};

class CommandAction : public AbstractAction
{
public:
    explicit CommandAction(const pugi::xml_node &cfgNode);
    ~CommandAction() override = default;
    void executeAction(const pugi::xml_node &payload) override;

private:
    std::string commandLine;
    static CommandExecutorThread *executorThread;
};

class CommandActionFactory :
    public AbstractActionFactory,
    public ntof::utils::Singleton<CommandActionFactory>
{
public:
    ~CommandActionFactory() override = default;
    AlarmActionPtr buildAction(const pugi::xml_node &cfgNode) override;

    static CommandActionFactory &init();

protected:
    friend class ntof::utils::Singleton<CommandActionFactory>;

    explicit CommandActionFactory(const std::string &name);
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* COMMANDACTION_H_ */
