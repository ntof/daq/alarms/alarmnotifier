/*
 * SpeechAction.h
 *
 *  Created on: May 22, 2018
 *      Author: mdonze
 */

#ifndef SPEECHACTION_H_
#define SPEECHACTION_H_

#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "AbstractAction.h"

namespace ntof {
namespace alarm {
namespace notifier {

class Speecher : public ntof::utils::Thread
{
public:
    static Speecher *getInstance();
    ~Speecher() override = default;
    void thread_enter() override;
    void thread_func() override;
    void say(std::string *text);
    void evalCommand(const std::string &cmd);

private:
    Speecher();
    std::vector<std::string> initCommands;
    ntof::utils::Queue<std::string *> speechQueue;
    static Speecher *instance;
};

class SpeechActionText
{
public:
    explicit SpeechActionText(const pugi::xml_node &cfgNode);
    virtual ~SpeechActionText() = default;
    std::string getText(const pugi::xml_node &dataNode);

private:
    enum TextType
    {
        NONE,
        PLAIN,
        ATTRIBUTE,
        NODE_TEXT
    };
    TextType type;
    std::string xPath;
    std::string elName;
};

class SpeechAction : public AbstractAction
{
public:
    explicit SpeechAction(const pugi::xml_node &cfgNode);
    ~SpeechAction() override = default;
    void executeAction(const pugi::xml_node &payload) override;

private:
    typedef std::shared_ptr<SpeechActionText> SpeechActionTextPtr;
    std::vector<SpeechActionTextPtr> texts;
};

class SpeechActionFactory :
    public AbstractActionFactory,
    public ntof::utils::Singleton<SpeechActionFactory>

{
public:
    ~SpeechActionFactory() override = default;
    AlarmActionPtr buildAction(const pugi::xml_node &cfgNode) override;
    void configureAction(const pugi::xml_node &cfgNode) override;

    static SpeechActionFactory &init();

protected:
    friend class ntof::utils::Singleton<SpeechActionFactory>;

    explicit SpeechActionFactory(const std::string &name);
};

} /* namespace notifier */
} /* namespace alarm */
} /* namespace ntof */

#endif /* SPEECHACTION_H_ */
