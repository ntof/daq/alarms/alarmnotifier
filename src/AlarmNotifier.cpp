/*
 * AlarmNotifier.cpp
 *
 *  Created on: May 14, 2018
 *      Author: mdonze
 */
#include "AlarmNotifier.h"

#include <chrono>
#include <iostream>
#include <thread>

#include <NTOFException.h>
#include <Timestamp.h>
#include <pugixml.hpp>
#include <pwd.h>

#include "AlarmClient.h"
#include "CommandAction.h"
#include "SoundAction.h"
#include "SpeechAction.h"
#include "StateClient.h"
#include "TimingClient.h"

namespace ntof {
namespace alarm {
namespace notifier {

AlarmNotifier::AlarmNotifier() : alarmClient(nullptr), timingClient(nullptr)
{
    // Init all the Action
    SpeechActionFactory::init();
    SoundActionFactory::init();
    CommandActionFactory::init();
}

AlarmNotifier::~AlarmNotifier()
{
    delete alarmClient;
}

[[noreturn]] void AlarmNotifier::runner(const std::string &configFile)
{
    m_configPath = configFile;
    loadConfiguration();
    connect();

    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        cleanUp();
    }
}

void AlarmNotifier::loadConfiguration()
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(m_configPath.c_str());
    if (!result)
    {
        throw ntof::NTOFException(
            "A problem occurred during the opening of the configuration file!",
            __FILE__, __LINE__);
    }

    pugi::xml_node rootNode = doc.child("configuration");
    if (!rootNode)
    {
        throw ntof::NTOFException("A problem occurred during the opening of "
                                  "the configuration file! (root missing)",
                                  __FILE__, __LINE__);
    }

    // Gets the global configuration node
    pugi::xml_node globNode = rootNode.child("global");

    // Gets default configurations for actions
    if (!globNode)
    {
        throw ntof::NTOFException(
            "A problem occurred during the opening of the configuration file!\n"
            "Global node not found!",
            __FILE__, __LINE__);
    }

    pugi::xml_node actCfgNode = globNode.child("actionsCfg");
    if (actCfgNode)
    {
        for (pugi::xml_node actCfg = actCfgNode.first_child(); actCfg;
             actCfg = actCfg.next_sibling())
        {
            std::string actName = actCfg.attribute("name").as_string();
            AbstractActionFactory::setActionConfiguration(actName, actCfg);
        }
    }
    std::string dimDnsNode =
        globNode.child("dimDns").attribute("value").as_string("");
    if (!dimDnsNode.empty())
    {
        DimClient::setDnsNode(dimDnsNode.c_str());
    }
    else
    {
        throw ntof::NTOFException(
            "A problem occurred during the opening of the configuration file!\n"
            "dimDNS node not found!",
            __FILE__, __LINE__);
    }

    // TODO: manage no-link alarm

    // Instantiate timing client (if any)
    pugi::xml_node timingNode = rootNode.child("timing");
    if (timingNode)
    {
        timingClient = new TimingClient(timingNode);
    }

    // Instantiate states clients
    pugi::xml_node statesNode = rootNode.child("states");
    for (pugi::xml_node state = statesNode.first_child(); state;
         state = state.next_sibling())
    {
        stateClients.push_back(new StateClient(state));
    }

    // Gets alarms definition
    pugi::xml_node alarmsNode = rootNode.child("alarms");
    alarmClient = new AlarmClient(alarmsNode);
}

/**
 * Performs clean-up. For being called in main thread
 */
void AlarmNotifier::cleanUp() {}

void AlarmNotifier::connect()
{
    if (alarmClient != nullptr)
    {
        alarmClient->connectToAlarms();
    }

    if (timingClient != nullptr)
    {
        timingClient->connectToTiming();
    }

    for (std::vector<StateClient *>::iterator it = stateClients.begin();
         it != stateClients.end(); ++it)
    {
        (*it)->connect();
    }
}

} // namespace notifier
} /* namespace alarm */
} /* namespace ntof */
